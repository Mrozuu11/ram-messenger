# RaM-Messenger

# The project is deployed and available to view [HERE](https://marcinmroz.me/ram-messenger)

This app is a simple messenger created with **Vue.JS**.
It utilises libraries such as **Axios** to access the **REST API** and **VuePersist** to handle the local storage. This Single Page Aplication allows the user to send messages to a range of characters pulled from the Rick and Morty API, and access the message history. The functionality includes displaying two views - New Message and History. The New Message view presents the user with a form to send a message (**form validation** included) whilst the History view stores the sent messages in **local storage**, sorts them (descending order) and displays them to the user.
The app is optimised for both desktop and mobile view.

Last but not least, I used tools such as **Docker** and **NGINX** to contenerise the project and create a server with reverse proxy to serve in production mode to my [DigitalOcean droplet](https://marcinmroz.me/ram-messenger)

## Stack:

- Vue.JS (Vue2)
- Vuex - store functionality
- Router - handling views
- Axios - working with the REST API
- VuePersist - handling local storage
- ElementUI
- SCSS

## Demo:

![RAM-demo.gif](src/assets/demo/RAM-demo.gif)

## Project setup

```

npm install

```

### Compiles and hot-reloads for development

```

npm run serve

```
