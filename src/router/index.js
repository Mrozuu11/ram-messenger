import Vue from "vue";
import VueRouter from "vue-router";
import NewMessage from "../views/NewMessage.vue";
import History from "../views/History.vue";
import ErrorPage from "@/components/ErrorPage/index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "New Message",
    component: NewMessage,
  },
  {
    path: "/history",
    name: "History",
    component: History,
  },
  { path: "*", name: 404, component: ErrorPage },
];

const router = new VueRouter({
  base: "/ram-messenger/",
  mode: "history",
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
